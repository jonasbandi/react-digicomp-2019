const Router = ReactRouterDOM.HashRouter;
const Route = ReactRouter.Route;
const Link = ReactRouterDOM.Link;
const Switch = ReactRouterDOM.Switch;

const Home = ({history}) => (
  <div>
    <h2>Home</h2>
    <button onClick={() => history.push('/about')}>Go</button>
  </div>
);

const About = (props) => (
  <div>
    <h2>About</h2>
    <div>Routing Information</div>
    <div>{JSON.stringify(props.match.params)}</div>
    <div>{props.match.isExact}</div>
    <div>{props.match.path}</div>
    <div>{props.match.url}</div>
  </div>
);

function App() {

  return (
    <Router>
      <div>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/about/42">About</Link>
          </li>
        </ul>

        <hr/>

        <Switch>
          <Route path="/about/:id" component={About}/>
          <Route path="/" component={Home}/>
        </Switch>
      </div>
    </Router>
  );
}

ReactDOM.render(
  <App/>,
  document.getElementById('root')
);


