import * as React from 'react';
import { TodoStore } from './store';
import { observer } from 'mobx-react';

type AppProps = { store: TodoStore };

export class App extends React.Component<AppProps> {

    render() {
        const {store} = this.props;
        return (
            <div>
                <h1>Todos</h1>
                <ul>
                    {
                        store.todos.map((todo, index) => <li key={index}>{todo.task}</li>  )
                    }
                </ul>
            </div>

        );
    }
}