import React, { Component } from 'react';
import { action, observable } from 'mobx';
import { observer } from 'mobx-react';

@observer
export class Counter extends Component {

    @observable count = 0;

    render() {
        return (
            <div>
                <h1>Count: {this.count}</h1>
                <button onClick={this.increase}>Increase</button>
            </div>
        );
    }

    @action
    increase = () => {
        this.count++;
    }
}
