const {useRef, useEffect} = React;

function GreeterComponent() {

  const buttonRef1 = useRef();
  const buttonRef2 = useRef();
  const buttonRef3 = useRef();

  useEffect(() => {
    buttonRef2.current.focus();
  }, []);

  return (
    <div>
      <h1>React works with a virtual DOM</h1>
      <button ref={buttonRef1}>
        Button 1
      </button>
      <br/>
      <button ref={buttonRef2}>
        Button 2
      </button>
      <br/>
      <button ref={buttonRef3}>
        Button 3
      </button>
    </div>
  );
}

ReactDOM.render(<GreeterComponent/>, document.getElementById("root"));

// DEMO:
// - create ref to title, apply jquery-fittext plugin:
// $(this.titleRef.current).fitText(1.5);
