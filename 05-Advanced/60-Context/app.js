const AppStateContext = React.createContext('app-state')

function ToDoList(){
  return (
    <AppStateContext.Consumer>
      {state => (
        <ul>
          {state.todos.map(t => (
            <li key={t.id}>{t.title}</li>
          ))}
        </ul>
      )}
    </AppStateContext.Consumer>
  )
}


function ToDoScreen(){
  return (
    <div>
      <h2>Pending Todos</h2>
      <ToDoList/>
    </div>
  )
}

class App extends React.Component {

  state = {todos: [{id: 1, title: 'Learn React'}]};

  render() {
    return (
      <AppStateContext.Provider value={this.state}>
        <ToDoScreen/>
      </AppStateContext.Provider>
    )
  }
}

ReactDOM.render(<App/>, document.getElementById('root'));
